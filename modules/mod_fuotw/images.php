<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Image Styles</title>
	<meta name="generator" content="BBEdit 10.1" />
	<style type="text/css">
	body {
		background-color: #e2e2e2;
	}
	.box {
	float:left;
	width:200px;
	margin:10px;
	text-align:center;
	border: 1px solid gray;
	padding: 8px;
	background-color: white;
	}
	h3 {
		font-size: 14px;
		line-height: 18px;
		font-family: Arial;
	}
	</style>
</head>
<body>
<div class="box">
<img src="/media/mod_fuotw/images/en/follow-us-on-twitter-1.png" />
<h3 style="text-align:center;">Style 1</h3>
</div>
<div class="box">
<img src="/media/mod_fuotw/images/en/follow-us-on-twitter-2.png" />
<h3 style="text-align:center;">Style 2</h3>
</div>
<div style="clear:both;"></div>
<div class="box">
<img src="/media/mod_fuotw/images/en/follow-us-on-twitter-3.png" />
<h3 style="text-align:center;">Style 3</h3>
</div>
<div class="box">
<img src="/media/mod_fuotw/images/en/follow-us-on-twitter-4.png" />
<h3 style="text-align:center;">Style 4</h3>
</div>
<div style="clear:both;"></div>
<div class="box">
<img src="/media/mod_fuotw/images/en/follow-us-on-twitter-5.png" />
<h3 style="text-align:center;">Style 5</h3>
</div>
<div class="box">
<img src="/media/mod_fuotw/images/en/follow-us-on-twitter-6.png" />
<h3 style="text-align:center;">Style 6</h3>
</div>
<div class="box">
<img src="/media/mod_fuotw/images/en/follow-us-on-twitter-7.png" />
<h3 style="text-align:center;">Style 7</h3>
</div>
<div class="box">
<img src="/media/mod_fuotw/images/en/follow-us-on-twitter-8.png" />
<h3 style="text-align:center;">Style 8</h3>
</div>
<div style="clear:both;"></div>
</body>
</html>
