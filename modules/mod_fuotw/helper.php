<?php
/**
* @version		$Id: helper.php 10381 2008-06-01 03:35:53Z pasamio $
* @package		Joomla
* @copyright	Copyright (C) 2012 River Media. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class modFUOTWHelper {

    public static function getFUOTWImage($image_text,$image_style,$language) {
    	$mod_URL = 'media/mod_fuotw/images/'.$language.'/';
    	$mod_path = 'media/mod_fuotw/images/'.$language.'/';
    	$img_URL = $mod_URL.'follow-us-on-twitter-'.$image_style.'.png';
    	$img_path = $mod_path.'follow-us-on-twitter-'.$image_style.'.png';
		$size = getimagesize(JPATH_BASE.'/'.$img_path); //get dimensions of image
    	$attr = array('title'=>$image_text,'width'=>$size[0],'height'=>$size[1]);
    	$img =  JHTML::image(JURI::base().$img_URL,'Follow Us On Twitter - Image',$attr);
	    
		return $img;
	}

    public static function getFUOTWCustomImage($image_text,$custom_image) {
    	$mod_URL = 'media/mod_fuotw/images/';
    	$mod_path = 'media/mod_fuotw/images/';
    	$img_URL = $mod_URL.$custom_image;
    	$img_path = $mod_path.$custom_image;
		$size = getimagesize(JPATH_BASE.'/'.$img_path); //get dimensions of image
    	$attr = array('title'=>$image_text,'width'=>$size[0],'height'=>$size[1]);
    	$img =  JHTML::image(JURI::base().$img_URL,'Follow Us On Twitter - Image',$attr);
	    
		return $img;
	}

}