<?php
/*------------------------------------------------------------------------
# mod_bizknizTwitterFeedDisplay - Bizkniz Twitter Feed Display
# ------------------------------------------------------------------------
# @author - Bizkniz
# copyright - All rights reserved by Bizkniz
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://bizkniz.net/
# Technical Support:  admin@bizkniz.net
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die;
require JModuleHelper::getLayoutPath('mod_bizknizTwitterFeedDisplay', $params->get('layout'));