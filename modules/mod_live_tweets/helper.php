<?php

/*------------------------------------------------------------------------
# mod__live_tweets - Live Tweets
# ------------------------------------------------------------------------
# author    Erik Maier
# copyright Copyright (C) 2010 rkmaier.com
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.rkmaier.com
# Technical Support: http://code.google.com/p/livetweeet/issues/list
-------------------------------------------------------------------------*/
// no direct access
defined('_JEXEC') or die('Restricted access');


class modLiveTweetsHelper 

{
	
	function buildURL($params,$type=true)  // Building the twitter search url for the twitter api
	{
		//Getting params
		
		$query=$params->get('query');
		$SearchOp=$params->get('SearchOp');  
		$lang=$params->get('language');
		$rpp=$params->get('rpp');
		$url='https://api.twitter.com/1.1/search/tweets.json?q=';
		if($type==false)
		{
		$url='https://api.twitter.com/1.1/search/tweets.atom?q=';
		}
		
		$word;

		switch($SearchOp)
		{
			case 0:
			 $word=$query;
			 break;
			case 1:
				$word='from%3A'.$query;
				break;
			case 2:
				$word='to%3A'.$query;
				break;
			case 3:
				$word='%40'.$query;
				break;

			case 4:
				$word='%23'.$query;
				break;
		}
		$rpp='&rpp='.$rpp;
		
		$fquery=$url.$word.$rpp;
		if($lang)
		{
			$lang='&lang='.$lang;
			$fquery=$fquery.$lang;
		}
		
		return $fquery;

	}
}

 //Using curl to retrieve json data

class curlconnector
{


    protected $_url;
    protected $_remoteFile;
    protected $_status;


    public function __construct($url)
    {
        $this->_url = $url;
        $this->useCurl();

    }


    protected function useCurl()
    {
        if ($session = curl_init($this->_url))
        {
            // Supress the HTTP headers
            curl_setopt($session, CURLOPT_HEADER, false);
            // Return the remote file as a string, rather than output it directly
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            // Get the remote file and store it in the $remoteFile property
            $this->_remoteFile = curl_exec($session);
            // Get the HTTP status
            $this->_status = curl_getinfo($session, CURLINFO_HTTP_CODE);

            // Close the cURL session
            curl_close($session);
        } else
        {
            $this->_error = 'Cannot establish cURL session';
        }
    }

    public function __toString()
    {
        if (!$this->_remoteFile)
        {
            $this->_remoteFile = '';
        }
        return $this->_remoteFile;
    }

    function &getInstance($query)
    {
    	static $instance;
    	if (!$instance)
    	{
    		$instance = new curlconnector($query);
    	}
    	return $instance;
    }
}





?>