<?php

/*------------------------------------------------------------------------
# mod__live_tweets - Live Tweets
# ------------------------------------------------------------------------
# author    Erik Maier
# copyright Copyright (C) 2010 rkmaier.com
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.rkmaier.com
# Technical Support: http://rkmaier.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

//Additional CSS and JS scripts
$doc =& JFactory::getDocument();
$doc->addStyleSheet('modules/mod_live_tweets/tmpl/css/live-tweet.css');
$doc->addScript('modules/mod_live_tweets/tmpl/scripts/utils.js');
$doc->addScript('modules/mod_live_tweets/tmpl/scripts/codebird.js');
$doc->addScript('modules/mod_live_tweets/tmpl/scripts/sha1.js');


$type=$params->get('type');
if($type==0)
    {
    $type="statuses_userTimeline";
    }
else
    {
    $type="search_tweets";
    }


$word=$params->get('query');
// Getting backend parameters

$height=$params->get('mheight')-4;
$container=$height-72;
$textwidht=$params->get('mwidth')-120;
if(!$params->get('avatar'))
{
	$textwidht=$textwidht+$params->get('avatarh');
	if($params->get('textwidth'))
	{
		$textwidht=$params->get('textwidth');
	}
	
}

if($params->get('twheight'))
{
	
	$twheight=$params->get('twheight');
}
$uname=$params->get('uname');
$count=$params->get('count');
$avatarh=$params->get('avatarh');
$fontsize=$params->get('fontsize');
$containerwidth=$params->get('mwidth')-12;
$followus=$params->get('followus');
$avatar=$params->get("avatar");
$lang=$params->get("lang");

//Getting API parameters

$cKey=$params->get("cKey");
$cSecret=$params->get("cSecret");
$aToken=$params->get("aToken");
$aTokenSecret=$params->get("aTokenSecret");

?>
<style>
#wrappall div.tweet-avatar img {
	width: <?php echo $avatarh ?>px;
	height: <?php echo $avatarh ?>px;
	float: left;
	padding: 2px;
	margin: 4px;
	border: 1px solid #ccc;
}
</style>

<div id="wrappall" style="width:<?php echo $params->get('mwidth')?>px; height:<?php echo $height ?>px">
	<div class="heading">
		
            <div id="followus" style="margin:7px 0 0 6px;">  
               <a href="https://twitter.com/<?php echo $followus;?>" class="twitter-follow-button" data-show-count="false" data-dnt="true">Follow @<?php echo $followus;?></a>
<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
</script>
		<!--<span class="hashtag"><?php echo ($word);?> </span>!-->
		<span id="loadJSON"><img src="modules/mod_live_tweets/tmpl/css/refresh.png" alt="Refresh"></img> </span>
	</div>
            </div>
	
	<div id='live-tweet-wrapper'style="padding-top:3px; height:<?php echo $container ?>px;width:<?php echo $containerwidth ?>px;">
        <span id="spiner" style="display:block;text-align:center;"> Refreshing...</span>
		<div id="tweets">

        </div>
        </div>
        <div class="heading">
        </div>
</div>


<script type="text/javascript">

window.addEvent('domready', function() {

var cb = new Codebird;
cb.setConsumerKey("<?php echo $cKey; ?>", "<?php echo $cSecret; ?>");
cb.setToken("<?php echo $aToken; ?>", "<?php echo $aTokenSecret; ?>");

$('loadJSON').addEvent('click', function(event){
$('spiner').fade("in");


$('tweets').set('html', '');    
var type = "<?php echo $type; ?>";

if(type=="search_tweets")
        {

var params = {
    count:"<?php echo $count; ?>",
    q:"<?php echo $word; ?>",
    lang:"<?php echo $lang;?>",
};
        }
        else
        {
var params = {
    count:"<?php echo $count; ?>",
    screen_name:"<?php echo $uname; ?>",
};

}
cb.__call(
    type,params,
    function (data) {
        if(type=="search_tweets")
        {
            var tweetdata = data.statuses;
        }
        else
        {
        var tweetdata = data;
        }
        var tweets = $('tweets');
        var TweetAvatar = '<?php echo $avatar; ?>';
        console.log(data);

        tweetdata.each(function(tweet,i){
            
            var tweetime = getTweetTime(tweet.created_at);

            var wrap = new Element('div',{
                'class':'tweet-wrap'
            }).inject(tweets,'bottom');
             if(TweetAvatar==1)
             {
            var TwAvatar = new Element('div',{
                html:'<img src="' + tweet.user.profile_image_url + '">',
                'class':'tweet-avatar'
            }).inject(wrap,'bottom');
        }

            var TwText = new Element('div',{
                html:'<div class="text" style="max-width:<?php echo $textwidht?>px;font-size:<?php echo $fontsize?>px"><span class="user">'+ tweet.user.screen_name + ' </span>'+ replaceURLWithHTMLLinks(tweet.text) +' <span class="grey"> via '+ html_entity_decode(tweet.source) + ' '  + tweetime +'</div>',
                'class':'tweet-text'
            }).inject(wrap,'bottom');
        });
        $('spiner').fade("out");
    }
);

});
$('loadJSON').fireEvent('click');
});
</script>
    

