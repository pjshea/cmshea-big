<?php

/*------------------------------------------------------------------------
# mod__live_tweets - Live Tweets
# ------------------------------------------------------------------------
# author    Erik Maier
# copyright Copyright (C) 2012 rkmaier.com
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.rkmaier.com
# Technical Support: rkmaier@rkmaier.co
-------------------------------------------------------------------------*/
// no direct access

defined('_JEXEC') or die('Restricted access');
JHTML::_( 'behavior.mootools' );

// Include the syndicate functions only once
require_once (dirname(__file__) . DS . 'helper.php');
require (JModuleHelper::getLayoutPath('mod_live_tweets'));


?>