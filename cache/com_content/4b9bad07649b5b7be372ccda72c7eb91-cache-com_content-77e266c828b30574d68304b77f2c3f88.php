<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:7452:"<div class="item-page" itemscope itemtype="http://schema.org/Article">
	<meta itemprop="inLanguage" content="en-GB" />
	
		
			<div class="page-header">
		<h2 itemprop="name">
							About the Run					</h2>
							</div>
							<div id="pop-print" class="btn hidden-print">
				<a href="#" onclick="window.print();return false;"><span class="icon-print"></span>Print</a>			</div>
			
	
	
		
								<div itemprop="articleBody">
		<h1>How the Run was Born</h1>
<p>The Annual Corporal Christopher M. Shea Memorial 5k was started by Chris' youngest brother, Timothy, with the help of Guy and Laurie Gordon and the Bears Running Club in the summer of 2005, the year after Chris' untimely death.</p>
<p>Tim really wanted to organize some type of charity event that would not only keep Chris' memory alive, but give back to the local community in a way that would make Chris proud. When Guy Gordon suggested a charity running event, Tim thought this was the perfect idea. Chris was a Cross Country runner in high school and for a short stint at Wagner College, ran his heart out in Marine Corp training and the Police Academy, and continued to keep himself in the best shape he could by running locally, and in charity events such as when he carried the torch each year to kickoff the Special Olympics in Lewes DE.</p>
<p>Each year, the Annual Corporal Chistopher M. Shea Memorial 5k attracts runners from across the tri-state area to compete. Awards are given to the top 3 male and female finishers overall and in 7 different age groups: 13 and under, 14-19, 20-29, 30-39, 40-49, 50-59, 60 and over.</p>
<p> </p>
<h1>The Battle of the Badges</h1>
<p>The "Battle of the Badges" is a unique race challenge that allows dedicated servicemen and women to demonstrate their commitment to their jobs and their community. The Battle of the Badges challenges the various civil service agencies to rally as many of their strongest, fastest, most committed and, most importantly, most competitive members to help remember a brother, serve a community and show everyone just how great they really are.</p>
<p>Agencies register as a team and compete against other teams during the race. Each teams results are scored using standard scoring methods for running events (top 5 times from each team) and the agency who achieves the best overall performance at the race will have their name engraved on the coveted Battle of the Badges plaque to be displayed at its headquarters until the following year when the Battle continues.</p>
<p> </p>
<h1 style="text-align: center;">Battle of the Badges Winners</h1>
<table style="margin-left: auto; margin-right: auto;" border="2" cellspacing="2" cellpadding="2">
<tbody>
<tr>
<td style="text-align: center; vertical-align: top;">
<h3>2015</h3>
<h4 style="line-height: 15.808px;">NJ State Police </h4>
<h4 style="line-height: 15.808px;">Field Ops</h4>
<p style="line-height: 15.808px;">Rob Rohel 18:53</p>
<p style="line-height: 15.808px;">Justin DeLorenzo 21:57</p>
<p style="line-height: 15.808px;">Andrew Stephanic 23:13</p>
</td>
<td style="text-align: center; vertical-align: top;">
<h3>2014</h3>
<h4 style="line-height: 15.808px;">NJ State Police North</h4>
<h4 style="line-height: 15.808px;">TEAMS</h4>
<p><span style="line-height: 15.808px;">Rob Rohel 18:56</span></p>
<p><span style="line-height: 15.808px;"><span style="line-height: 15.808px;">Brian Santos</span> 19:36</span></p>
<span style="line-height: 15.808px;">Tommy Ferrigno 19:45</span><br />
<p><span style="line-height: 15.808px;"><span style="line-height: 15.808px;">Brian Santos 25:55</span> </span></p>
</td>
<td style="text-align: center; vertical-align: top;">
<h3> <span style="line-height: 1.3em;">2013</span></h3>
<h4> NJ State Police Troop B  </h4>
<h4>Washington Station</h4>
<p>Brian Santos 19:32</p>
<p>Mark Moyna 20:01</p>
<p>Justin DeLorenzo 22:10</p>
<p>David Ortiz 25:47</p>
</td>
</tr>
<tr>
<td style="text-align: center; vertical-align: top;">
<h3>2012</h3>
<h4>NJ State Police Troop B</h4>
<h4>Hope Station</h4>
<p style="line-height: 15.808px;">Mark Moyna 19:21</p>
<p style="line-height: 15.808px;">Justin DeLorenzo 20:59</p>
<p style="line-height: 15.808px;">Andrew Stephanic 21:00</p>
<p style="line-height: 15.808px;">James Agens 23:07</p>
</td>
<td style="text-align: center; vertical-align: top;">
<h3>2011</h3>
<h4> NJ State Police Troop B  </h4>
<h4>Hope Station</h4>
<p><span style="line-height: 15.808px;">Mark Moyna 20:00</span></p>
<p>James Agens 22:28</p>
</td>
<td style="text-align: center; vertical-align: top;">
<h3>2010</h3>
<h4> NJ State Police Troop B  </h4>
<h4>TEAMS</h4>
<p style="font-family: Tahoma, Helvetica, Arial, sans-serif; font-size: 12px;"><span style="line-height: 1.3em;">Rob Rohel 17:53</span></p>
<p style="color: #333333; font-family: Tahoma, Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal;"><span style="line-height: 1.3em;">Tommy Ferrigno 20:10</span></p>
<p style="color: #333333; font-family: Tahoma, Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal;"><span style="line-height: 1.3em;">Joe Trogani 21:56</span></p>
<span style="line-height: 1.3em;"> </span> </td>
</tr>
<tr>
<td style="text-align: center; vertical-align: top;">
<h3>2009</h3>
<h4> NJ State Police Troop B  </h4>
<h4>Hope Station</h4>
<p style="line-height: 15.808px;">Mark Moyna 21:29</p>
<p style="line-height: 15.808px;">Justin DeLorenzo 21:03</p>
</td>
<td style="text-align: center; vertical-align: top;">
<h3>2008</h3>
<h4>NJ State Police Troop B  </h4>
<h4>TEAMS</h4>
<p style="font-family: Tahoma, Helvetica, Arial, sans-serif; font-size: 12px;"><span style="line-height: 1.3em;">Rob Rohel 18:26</span></p>
<p style="color: #333333; font-family: Tahoma, Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal;"><span style="line-height: 1.3em;">Tommy Ferrigno 19:37</span> </p>
</td>
<td style="text-align: center; vertical-align: top;">
<h3>2007</h3>
<h4> NJ State Police Troop D  </h4>
<span style="font-family: Tahoma, Helvetica, Arial, sans-serif; font-size: 12px; line-height: 1.3em;">Tommy Ferrigno 19:55.1</span> </td>
</tr>
</tbody>
</table>
<p> </p> <div class="addtoany_container"><span class="a2a_kit a2a_kit_size_32 addtoany_list" data-a2a-url="http://cmshea.org/about-the-run" data-a2a-title="About the Run">
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_google_plus"></a>
<a class="a2a_dd" href="http://www.addtoany.com/share_save"></a>
</span>
</div>	</div>

				<dl class="article-info muted">

		
			<dt class="article-info-term">
													Details							</dt>

							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
					Written by <span itemprop="name">Patrick Shea</span>	</dd>
			
			
										<dd class="category-name">
																		Category: <a href="/about-the-run/2-uncategorised" itemprop="genre">Uncategorised</a>							</dd>			
										<dd class="published">
				<span class="icon-calendar"></span>
				<time datetime="2014-07-18T04:57:35+00:00" itemprop="datePublished">
					Published: 18 July 2014				</time>
			</dd>					
					
			
										<dd class="hits">
					<span class="icon-eye-open"></span>
					<meta itemprop="interactionCount" content="UserPageVisits:1575" />
					Hits: 1575			</dd>						</dl>
	
						</div>

";s:4:"head";a:11:{s:5:"title";s:7:"The Run";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:4:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:12:"Patrick Shea";s:6:"robots";s:17:"noindex, nofollow";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:54:"/components/com_jcomments/tpl/default/style.css?v=3002";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:55:"http://cmshea.org/plugins/content/addtoany/addtoany.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:7:{s:51:"/components/com_jcomments/js/jcomments-v2.3.js?v=12";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:58:"/components/com_jcomments/libraries/joomlatune/ajax.js?v=4";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:34:"//static.addtoany.com/menu/page.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/jui/js/jquery.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:34:"/media/jui/js/jquery-noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:35:"/media/jui/js/jquery-migrate.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:80:"jQuery(window).on('load',  function() {
				new JCaption('img.caption');
			});";}s:6:"custom";a:0:{}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:7:"The Run";s:4:"link";s:20:"index.php?Itemid=109";}}s:6:"module";a:0:{}}