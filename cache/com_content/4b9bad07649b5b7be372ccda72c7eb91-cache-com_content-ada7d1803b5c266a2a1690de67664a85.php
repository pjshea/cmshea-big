<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:36861:"<div class="blog" itemscope itemtype="http://schema.org/Blog">
	
	
	
	
	
				<div class="items-leading clearfix">
							<div class="leading-0"
					itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
					

			<div class="page-header">

							<h2 itemprop="name">
											<a href="/" itemprop="url">
						11th Annual Corporal Christopher M. Shea Memorial 5k Recap</a>
									</h2>
			
											</div>
	
	
<div class="icons">
	
					<div class="btn-group pull-right">
				<a class="btn dropdown-toggle" data-toggle="dropdown" href="#"> <span class="icon-cog"></span><span class="caret"></span> </a>
								<ul class="dropdown-menu">
											<li class="print-icon"> <a href="/?tmpl=component&amp;print=1&amp;page=" title="Print" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon-print"></span>Print</a> </li>
																<li class="email-icon"> <a href="/component/mailto/?tmpl=component&amp;template=protostar_cmshea&amp;link=c088391dc4d85aa4e7808716666c4e60b242596f" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;" rel="nofollow"><span class="icon-envelope"></span>Email</a> </li>
														</ul>
			</div>
		
	</div>






 <p><img src="images/pictures/2015/IMG_3042.JPG" alt="" width="1050" height="700" /></p>
<p style="font-size: 12.16px; line-height: 15.808px;">The <strong>11th Annual Corporal Christopher M. Shea Memorial 5k</strong> was held on October 4, 2015 in<strong> </strong>Swartswood State Park. The event was a success, with 100+ runners and walkers, and spectators filling the pavilion to cheer on the race finishers. It was a gorgeous day for a run, jog, or even a walk through the park.</p>
<p style="font-size: 12.16px; line-height: 15.808px;">The top 3 overall male finishers were: Rob Rohel from Wantage NJ (18:53), Jiim Kline from Blairstown NJ (19:12), and Carmen Liuzza (19:45).</p>
<p style="font-size: 12.16px; line-height: 15.808px;"><span style="line-height: 15.8079996109009px;">The top 3 overall female finishers were: Christine Chapman from Hopatcong NJ (22:12), Meghan Radimer (22:57), and Ashley Espinosa (23:10).</span></p>
<p style="font-size: 12.16px; line-height: 15.808px;">We'd like to also say that the competition for the <b>Battle of the Badges</b> this awesome. There were runners from the NJ State Police and the Delaware State Police competing to take home the coveted <strong>Battle of the Badges</strong> plaque. In the end, the team of Rob Rohel, Justin DeLorenzo, and Andrew Stephanic of the NJSP Field Ops out the DE State Police score of 17-19 (lowest score wins).</p>
<p style="font-size: 12.16px; line-height: 15.808px;">Congratulations to our top-finishers, all our winners, and everyone that came out to run, jog, walk, spectate, and volunteer in support of the <strong>Corporal Christopher M. Shea Memorial Scholarship Fund</strong> - thank you for making this year competitive, fun, exciting, and for helping us raise over $4,000 to be used as scholarships for students from Kittatinny Regional High School, Sussex County Community College, and other area schools entering a career in law enforcement or as other civil servants.</p>
<p style="font-size: 12.16px; line-height: 15.808px;">We'd also like to thank all of our sponsors and partners - without the help of our primary sponsors Daidone Electric, Inc. and Accomplitech, and all other donors and sponsors we wouldn't have been able to keep this race alive for these past 10 years.  </p>
<p style="color: blue;">Mark your calendars now for next year! </p>
<div id="header" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 18px;"><strong>12th Annual <span style="font-family: Tahoma, Helvetica, Arial, sans-serif; font-size: 12px; line-height: 1.3em;">Corporal Christopher M. Shea Memorial 5k</span></strong></div>
<div style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 18px;"><strong>When: </strong>Sunday, October 2nd 2016 @ 1 PM</div>
<div style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 18px;"><strong>Where: </strong>Swartswood State Park, Swartswood, NJ</div><div class="addtoany_container"><span class="a2a_kit a2a_kit_size_32 addtoany_list" data-a2a-url="http://www.cmshea.org/" data-a2a-title="11th Annual Corporal Christopher M. Shea Memorial 5k Recap">
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_google_plus"></a>
<a class="a2a_dd" href="http://www.addtoany.com/share_save"></a>
</span>
</div>
		<dl class="article-info muted">

		
			<dt class="article-info-term">
													Details							</dt>

							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
					Written by <span itemprop="name">Patrick Shea</span>	</dd>
			
			
										<dd class="category-name">
																		Category: <a href="/race-results/2-uncategorised" itemprop="genre">Uncategorised</a>							</dd>			
										<dd class="published">
				<span class="icon-calendar"></span>
				<time datetime="2015-10-28T01:47:59+00:00" itemprop="datePublished">
					Published: 28 October 2015				</time>
			</dd>					
					
			
										<dd class="hits">
					<span class="icon-eye-open"></span>
					<meta itemprop="interactionCount" content="UserPageVisits:2121" />
					Hits: 2121			</dd>						</dl>



				</div>
									</div><!-- end items-leading -->
	
	
																	<div class="items-row cols-2 row-0 row-fluid clearfix">
						<div class="span6">
				<div class="item column-1"
					itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
					

			<div class="page-header">

							<h2 itemprop="name">
											<a href="/chris-story" itemprop="url">
						The Life of a Hero</a>
									</h2>
			
											</div>
	
	
<div class="icons">
	
					<div class="btn-group pull-right">
				<a class="btn dropdown-toggle" data-toggle="dropdown" href="#"> <span class="icon-cog"></span><span class="caret"></span> </a>
								<ul class="dropdown-menu">
											<li class="print-icon"> <a href="/chris-story?tmpl=component&amp;print=1&amp;page=" title="Print" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon-print"></span>Print</a> </li>
																<li class="email-icon"> <a href="/component/mailto/?tmpl=component&amp;template=protostar_cmshea&amp;link=051f07f2e9f37c6cef3ff9ccc595da7fb9afc29f" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;" rel="nofollow"><span class="icon-envelope"></span>Email</a> </li>
														</ul>
			</div>
		
	</div>






 <h1>The Beginning</h1>
<hr />
<p>Corporal Christopher M. Shea was born on Oct. 4th, 1972 to Maurice and Patricia Shea of Jersey City, NJ. He was the second child in a family of eight. Just before he entered into the 4th grade, the family moved to Union City, NJ, where Chris spent his youth days attending St. Joseph's school (4th to the 8th grades). The summer before Chris entered high school, the family chose to leave the urban areas of NJ, and raise the growing family (the 8th, Timothy, was just born) in rural Sussex County, NJ.</p>
<p>Chris attended Kittatinny Regional High School in Hampton, NJ. He ran cross country in the fall, played basketball in the winter, and played baseball in the spring. He loved imposing his techniques as a catcher on anyone that would listen. He graduated in 1990, with a full understanding of his career goals.</p>
<p>Chris' passion to become a police officer started when he was 12. In the eighth grade, Chris was nominated to spend a week at the NJ State Police basic training facility in Sea Girt, and this is what convinced him that this is what he wanted to do. As the end of high school approached, and Chris would have to choose his career direction, is when Chris would begin to turn this dream into a reality. He would first choose Wagner College in Staten Island, NY, where he attempted to study Criminal Justice. Realizing after his first semester that this studying "gig" was not for him, and knowing at the time that the only other path to becoming a NJ State Police Officer without a college degree was 4 years of military service, Chris chose to become one of the few and the proud and enlisted in the United States Marine Corps.</p>
<p> </p>
<h1>The Marines</h1>
<hr />
<p>Chris served 4 glorious years in the Marine Corps. He worked as a security officer in what is now most likely considered Homeland Security. This was fortunate for him and his family, since it limited his assignment to a military base in Jacksonville, FL and he never had to do a global tour or serve over seas during Desert Storm.</p>
<p>Chris loved the Marine Corps, and he served his duty guarding nuclear missiles used on submarines with conviction and pride.</p>
<p> </p>
<h1>His Family</h1>
<hr />
<p>After Chris completed his tour in the military, he took a job as a security officer at Fairleigh Dickinson University in Teaneck, NJ. This worked well, you see, because Chris had come to learn that the requirement for being accepted into NJ State Police had changed while he was serving his 4 years in the Marines. When he enlisted, the requirement was 4 years of military service OR a 4-yr college degree. At this point, the rules were now 4 years of military service AND at least a 2-yr associates degree. Chris quickly enrolled into the FDU curriculum to get started.</p>
<p>Chris knew he would get through this program and earn his degree (I'm not sure he expected to do it with honors), but what he didn't know was that a whole new part of his life would start to take shape at FDU, as well. Her name was Susan Cleaver, a varsity volleyball player at FDU. When she first laid eyes on Chris in the summer of 1995, she emphatically told her twin sister, Shannon, that "I'm going to get that man - you'll see". Did she ever.</p>
<p>Chris proposed to Susan in front of an entire gym of FDU volleyball players and fans. Quickly changing into a suit as the end of the match neared, Chris had his buddies display a HUGE sign reading "Susan, Will You Marry Me" as he approached Susan with roses and an engagement ring. Getting on one knee, Chris proposed and Susan gladly accepted.</p>
<p>Chris and Susan were married on May 15, 1999 in Lewes, DE. Their son, Christopher Jr., was born on January 16, 2001. Just like any other proud father, this filled Chris' passion to have a first born son. Their daughter, Elizabeth, was born on August 8 2003. The spitting image of Chris, she quickly became daddy's little girl.</p>
<p> </p>
<h1>The Delaware State Police</h1>
<hr />
<p>Chris moved to Delaware in the winter in the early part of 1999, shortly after he graduated from Fairleigh. He worked a few odd jobs while waiting through the DE State Police application process -- a security officer at Ames department store and a corrections officer. He finally got his chance in 2000, when he was accepted into DE State Police Academy to be part of the 71st Delaware State Police Recruit Class.</p>
<p>Chris graduated from the Delaware State Police Academy on December 16, 2000. He was first assigned to Troop 5, in Bridgeville DE, off the Sussex Highway and about 30 miles west of where he and Susan lived. Saying he was excited to finally be "on the job" is an understatement. I'll always remember the countless hours talking to Chris about his dream job, or more preciseley listening to him ramble on with story after story of his daily routines.</p>
<p>On one particular call, we all got a BIG scare. Chris was leaving the Troop 5 barracks in response to a call, crossing over Sussex Highway as he exited the barracks. On his way out, his patrol car was broad sided by some sort of delivery van. I don't like to think of Chris as lucky -- instead I think of him as resilient. He ended up with a broken hip and he still managed to serve as a groomsman (well, on crutches) at our oldest brother's wedding. He kept a smile on his face, quick to say to anyone about to inquire that "No, he did not have to pay for the patrol car -- that's what taxes are for".</p>
<p>Chris eventually capitilized on an opportunity to transfer to DE Troop 7, based in Lewes where he and his family lived. Chris welcomed the short commute to work, and loved the idea that he was only a stones throw away from home. Chris was determined to work his way up the ranks, so he continued to accept overtime work whenever it was available, and the fact that he was now so close to home meant that he would make it home shortly after his shift was over. His hard work and dedication paid off in July of 2004 when he was promoted from Trooper First Class to Corporal. Chris was killed that same month.</p>
<p>It was July 18, 2004 at approximately 2 AM, and he was returning home from a late overtime job on his day off. The job was a routine criminal dropoff to the corrections facility in DE, north of Lewes. It was something he figured was a quick few hours of work, no problem. What he didn't expect to encounter was the unthinkable - a driver on the wrong side of the highway.</p>
<p>Chris was travelling southbound on State Route 1 near Route 206 just outside Milford. The other vehicle was headed northbound in the southbound lanes. The car was being operated by a drunk driver, and was fleeing the scene of an accident that occurred shortly before the fatal crash. Chris never saw it coming - he was taken to Milford Memorial Hospital and pronounced dead at 2:54 AM.</p>
<p>Corporal Christopher M. Shea was laid to rest on July 22, 2004 with full military honors. The funeral services were attended by family, friends, troopers and police officers from across the U.S. The services were powerful, like nothing I have ever experienced before and hope to never have to experience again.</p>
<p>Chris is a hero. A hero because he chose a career, a lifestyle, of helping others. He was never afraid of what he was doing, never second guessed himself for one second. During the funeral services, Chris' Academy classmate and friend of 4 years gave a personal and moving <a href="images/eulogy.jpg">eulogy</a>where he gave, perhaps, the best reason for why Chris was a hero. He said "Chris, I believe you are a hero who saved the life of an unknown motorist who would have been travelling southbound on State Route 1 by forfeiting your own life. Your selfless service saved the life of someone you never even met."</p><div class="addtoany_container"><span class="a2a_kit a2a_kit_size_32 addtoany_list" data-a2a-url="http://www.cmshea.org/chris-story" data-a2a-title="The Life of a Hero">
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_google_plus"></a>
<a class="a2a_dd" href="http://www.addtoany.com/share_save"></a>
</span>
</div>
		<dl class="article-info muted">

		
			<dt class="article-info-term">
													Details							</dt>

							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
					Written by <span itemprop="name">Patrick Shea</span>	</dd>
			
			
										<dd class="category-name">
																		Category: <a href="/race-results/2-uncategorised" itemprop="genre">Uncategorised</a>							</dd>			
										<dd class="published">
				<span class="icon-calendar"></span>
				<time datetime="2014-07-18T04:54:12+00:00" itemprop="datePublished">
					Published: 18 July 2014				</time>
			</dd>					
					
			
										<dd class="hits">
					<span class="icon-eye-open"></span>
					<meta itemprop="interactionCount" content="UserPageVisits:2438" />
					Hits: 2438			</dd>						</dl>



				</div>
				<!-- end item -->
							</div><!-- end span -->
														<div class="span6">
				<div class="item column-2"
					itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
					

			<div class="page-header">

							<h2 itemprop="name">
											<a href="/about-the-scholarship" itemprop="url">
						About the Scholarship</a>
									</h2>
			
											</div>
	
	
<div class="icons">
	
					<div class="btn-group pull-right">
				<a class="btn dropdown-toggle" data-toggle="dropdown" href="#"> <span class="icon-cog"></span><span class="caret"></span> </a>
								<ul class="dropdown-menu">
											<li class="print-icon"> <a href="/about-the-scholarship?tmpl=component&amp;print=1&amp;page=" title="Print" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon-print"></span>Print</a> </li>
																<li class="email-icon"> <a href="/component/mailto/?tmpl=component&amp;template=protostar_cmshea&amp;link=503664adc864c12a4c6beaf08ef1f6604279c0db" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;" rel="nofollow"><span class="icon-envelope"></span>Email</a> </li>
														</ul>
			</div>
		
	</div>






 <div class="mainFlat"><a href="docs/CMShea_Scholarship_App_1.2.pdf">Apply for the Scholarship</a></div>
<div class="mainFlat"><hr /></div>
<h2 class="mainFlat"> </h2>
<table style="margin-left: auto; margin-right: auto;" border="1">
<tbody>
<tr>
<td style="text-align: center;" colspan="3">
<p><span style="font-weight: bold;">Corporal Christopher M. Shea Scholarship Award Recipients</span></p>
</td>
</tr>
<tr>
<td style="text-align: center;">
<p><span style="font-weight: bold; text-align: center;">Year </span></p>
</td>
<td style="text-align: center;">
<p><span style="font-weight: bold; text-align: center;">Recipient</span></p>
</td>
<td style="text-align: center;">
<p><span style="font-weight: bold; text-align: center;">High School</span></p>
</td>
</tr>
<tr>
<td>
<p>2015</p>
</td>
<td>
<p>Jordan Lorenzo</p>
<p>TBD</p>
</td>
<td>
<p><span style="line-height: 15.808px;">Kittatinny Regional, Hampton NJ</span></p>
<p><span style="line-height: 15.808px;">Sussex County Community College</span></p>
</td>
</tr>
<tr>
<td>
<p>2014</p>
</td>
<td>
<p>Blake E. DiGiaimo and Scott H. Sponder</p>
</td>
<td>
<p>Kittatinny Regional, Hampton NJ </p>
</td>
</tr>
<tr>
<td>
<p>2013</p>
</td>
<td>
<p>Jeffrey M. Chandler</p>
</td>
<td>
<p>Kittatinny Regional, Hampton NJ</p>
</td>
</tr>
<tr>
<td>
<p>2012</p>
</td>
<td>
<p>Conner T. Casterline and Jordan P. Murch</p>
</td>
<td>
<p>Kittatinny Regional, Hampton NJ</p>
</td>
</tr>
<tr>
<td>
<p>2011</p>
</td>
<td>
<p>Donated to the Ryan Senkier Fight Cancer Fund  </p>
<p>http://helpryanfight.wordpress.com/</p>
</td>
<td>
<p>Ryan was a 1992 graduate of Kittatinny Regional High School </p>
</td>
</tr>
<tr>
<td>
<p>2010</p>
</td>
<td>
<p>Daniel McCarthy and Emileigh Galinski</p>
</td>
<td>
<p>Kittatinny Regional, Hampton NJ</p>
</td>
</tr>
<tr>
<td>
<p>2009</p>
</td>
<td>
<p>Jeff Latawiec and Stacey Norman</p>
</td>
<td>
<p>Kittatinny Regional, Hampton NJ</p>
</td>
</tr>
<tr>
<td>
<p>2008</p>
</td>
<td>
<p>Jacob Stark</p>
</td>
<td>
<p>Kittatinny Regional, Hampton NJ</p>
</td>
</tr>
<tr>
<td>
<p>2007</p>
</td>
<td>
<p>Zachary Virga</p>
</td>
<td>
<p>Kittatinny Regional, Hampton NJ</p>
</td>
</tr>
</tbody>
</table><div class="addtoany_container"><span class="a2a_kit a2a_kit_size_32 addtoany_list" data-a2a-url="http://www.cmshea.org/about-the-scholarship" data-a2a-title="About the Scholarship">
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_google_plus"></a>
<a class="a2a_dd" href="http://www.addtoany.com/share_save"></a>
</span>
</div>
		<dl class="article-info muted">

		
			<dt class="article-info-term">
													Details							</dt>

							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
					Written by <span itemprop="name">Patrick Shea</span>	</dd>
			
			
										<dd class="category-name">
																		Category: <a href="/race-results/2-uncategorised" itemprop="genre">Uncategorised</a>							</dd>			
										<dd class="published">
				<span class="icon-calendar"></span>
				<time datetime="2014-07-22T18:46:22+00:00" itemprop="datePublished">
					Published: 22 July 2014				</time>
			</dd>					
					
			
										<dd class="hits">
					<span class="icon-eye-open"></span>
					<meta itemprop="interactionCount" content="UserPageVisits:1533" />
					Hits: 1533			</dd>						</dl>



				</div>
				<!-- end item -->
							</div><!-- end span -->
							</div><!-- end row -->
																			<div class="items-row cols-2 row-1 row-fluid clearfix">
						<div class="span6">
				<div class="item column-1"
					itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
					

			<div class="page-header">

							<h2 itemprop="name">
											<a href="/about-the-run" itemprop="url">
						About the Run</a>
									</h2>
			
											</div>
	
	
<div class="icons">
	
					<div class="btn-group pull-right">
				<a class="btn dropdown-toggle" data-toggle="dropdown" href="#"> <span class="icon-cog"></span><span class="caret"></span> </a>
								<ul class="dropdown-menu">
											<li class="print-icon"> <a href="/about-the-run?tmpl=component&amp;print=1&amp;page=" title="Print" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon-print"></span>Print</a> </li>
																<li class="email-icon"> <a href="/component/mailto/?tmpl=component&amp;template=protostar_cmshea&amp;link=be1b78928c26d33319f01438e64673cbe899e737" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;" rel="nofollow"><span class="icon-envelope"></span>Email</a> </li>
														</ul>
			</div>
		
	</div>






 <h1>How the Run was Born</h1>
<p>The Annual Corporal Christopher M. Shea Memorial 5k was started by Chris' youngest brother, Timothy, with the help of Guy and Laurie Gordon and the Bears Running Club in the summer of 2005, the year after Chris' untimely death.</p>
<p>Tim really wanted to organize some type of charity event that would not only keep Chris' memory alive, but give back to the local community in a way that would make Chris proud. When Guy Gordon suggested a charity running event, Tim thought this was the perfect idea. Chris was a Cross Country runner in high school and for a short stint at Wagner College, ran his heart out in Marine Corp training and the Police Academy, and continued to keep himself in the best shape he could by running locally, and in charity events such as when he carried the torch each year to kickoff the Special Olympics in Lewes DE.</p>
<p>Each year, the Annual Corporal Chistopher M. Shea Memorial 5k attracts runners from across the tri-state area to compete. Awards are given to the top 3 male and female finishers overall and in 7 different age groups: 13 and under, 14-19, 20-29, 30-39, 40-49, 50-59, 60 and over.</p>
<p> </p>
<h1>The Battle of the Badges</h1>
<p>The "Battle of the Badges" is a unique race challenge that allows dedicated servicemen and women to demonstrate their commitment to their jobs and their community. The Battle of the Badges challenges the various civil service agencies to rally as many of their strongest, fastest, most committed and, most importantly, most competitive members to help remember a brother, serve a community and show everyone just how great they really are.</p>
<p>Agencies register as a team and compete against other teams during the race. Each teams results are scored using standard scoring methods for running events (top 5 times from each team) and the agency who achieves the best overall performance at the race will have their name engraved on the coveted Battle of the Badges plaque to be displayed at its headquarters until the following year when the Battle continues.</p>
<p> </p>
<h1 style="text-align: center;">Battle of the Badges Winners</h1>
<table style="margin-left: auto; margin-right: auto;" border="2" cellspacing="2" cellpadding="2">
<tbody>
<tr>
<td style="text-align: center; vertical-align: top;">
<h3>2015</h3>
<h4 style="line-height: 15.808px;">NJ State Police </h4>
<h4 style="line-height: 15.808px;">Field Ops</h4>
<p style="line-height: 15.808px;">Rob Rohel 18:53</p>
<p style="line-height: 15.808px;">Justin DeLorenzo 21:57</p>
<p style="line-height: 15.808px;">Andrew Stephanic 23:13</p>
</td>
<td style="text-align: center; vertical-align: top;">
<h3>2014</h3>
<h4 style="line-height: 15.808px;">NJ State Police North</h4>
<h4 style="line-height: 15.808px;">TEAMS</h4>
<p><span style="line-height: 15.808px;">Rob Rohel 18:56</span></p>
<p><span style="line-height: 15.808px;"><span style="line-height: 15.808px;">Brian Santos</span> 19:36</span></p>
<span style="line-height: 15.808px;">Tommy Ferrigno 19:45</span><br />
<p><span style="line-height: 15.808px;"><span style="line-height: 15.808px;">Brian Santos 25:55</span> </span></p>
</td>
<td style="text-align: center; vertical-align: top;">
<h3> <span style="line-height: 1.3em;">2013</span></h3>
<h4> NJ State Police Troop B  </h4>
<h4>Washington Station</h4>
<p>Brian Santos 19:32</p>
<p>Mark Moyna 20:01</p>
<p>Justin DeLorenzo 22:10</p>
<p>David Ortiz 25:47</p>
</td>
</tr>
<tr>
<td style="text-align: center; vertical-align: top;">
<h3>2012</h3>
<h4>NJ State Police Troop B</h4>
<h4>Hope Station</h4>
<p style="line-height: 15.808px;">Mark Moyna 19:21</p>
<p style="line-height: 15.808px;">Justin DeLorenzo 20:59</p>
<p style="line-height: 15.808px;">Andrew Stephanic 21:00</p>
<p style="line-height: 15.808px;">James Agens 23:07</p>
</td>
<td style="text-align: center; vertical-align: top;">
<h3>2011</h3>
<h4> NJ State Police Troop B  </h4>
<h4>Hope Station</h4>
<p><span style="line-height: 15.808px;">Mark Moyna 20:00</span></p>
<p>James Agens 22:28</p>
</td>
<td style="text-align: center; vertical-align: top;">
<h3>2010</h3>
<h4> NJ State Police Troop B  </h4>
<h4>TEAMS</h4>
<p style="font-family: Tahoma, Helvetica, Arial, sans-serif; font-size: 12px;"><span style="line-height: 1.3em;">Rob Rohel 17:53</span></p>
<p style="color: #333333; font-family: Tahoma, Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal;"><span style="line-height: 1.3em;">Tommy Ferrigno 20:10</span></p>
<p style="color: #333333; font-family: Tahoma, Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal;"><span style="line-height: 1.3em;">Joe Trogani 21:56</span></p>
<span style="line-height: 1.3em;"> </span> </td>
</tr>
<tr>
<td style="text-align: center; vertical-align: top;">
<h3>2009</h3>
<h4> NJ State Police Troop B  </h4>
<h4>Hope Station</h4>
<p style="line-height: 15.808px;">Mark Moyna 21:29</p>
<p style="line-height: 15.808px;">Justin DeLorenzo 21:03</p>
</td>
<td style="text-align: center; vertical-align: top;">
<h3>2008</h3>
<h4>NJ State Police Troop B  </h4>
<h4>TEAMS</h4>
<p style="font-family: Tahoma, Helvetica, Arial, sans-serif; font-size: 12px;"><span style="line-height: 1.3em;">Rob Rohel 18:26</span></p>
<p style="color: #333333; font-family: Tahoma, Helvetica, Arial, sans-serif; font-size: 12px; font-weight: normal;"><span style="line-height: 1.3em;">Tommy Ferrigno 19:37</span> </p>
</td>
<td style="text-align: center; vertical-align: top;">
<h3>2007</h3>
<h4> NJ State Police Troop D  </h4>
<span style="font-family: Tahoma, Helvetica, Arial, sans-serif; font-size: 12px; line-height: 1.3em;">Tommy Ferrigno 19:55.1</span> </td>
</tr>
</tbody>
</table>
<p> </p><div class="addtoany_container"><span class="a2a_kit a2a_kit_size_32 addtoany_list" data-a2a-url="http://www.cmshea.org/about-the-run" data-a2a-title="About the Run">
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_google_plus"></a>
<a class="a2a_dd" href="http://www.addtoany.com/share_save"></a>
</span>
</div>
		<dl class="article-info muted">

		
			<dt class="article-info-term">
													Details							</dt>

							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
					Written by <span itemprop="name">Patrick Shea</span>	</dd>
			
			
										<dd class="category-name">
																		Category: <a href="/race-results/2-uncategorised" itemprop="genre">Uncategorised</a>							</dd>			
										<dd class="published">
				<span class="icon-calendar"></span>
				<time datetime="2014-07-18T04:57:35+00:00" itemprop="datePublished">
					Published: 18 July 2014				</time>
			</dd>					
					
			
										<dd class="hits">
					<span class="icon-eye-open"></span>
					<meta itemprop="interactionCount" content="UserPageVisits:1575" />
					Hits: 1575			</dd>						</dl>



				</div>
				<!-- end item -->
							</div><!-- end span -->
														<div class="span6">
				<div class="item column-2"
					itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
					

			<div class="page-header">

							<h2 itemprop="name">
											<a href="/how-can-i-help" itemprop="url">
						How Can I Help?</a>
									</h2>
			
											</div>
	
	
<div class="icons">
	
					<div class="btn-group pull-right">
				<a class="btn dropdown-toggle" data-toggle="dropdown" href="#"> <span class="icon-cog"></span><span class="caret"></span> </a>
								<ul class="dropdown-menu">
											<li class="print-icon"> <a href="/how-can-i-help?tmpl=component&amp;print=1&amp;page=" title="Print" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon-print"></span>Print</a> </li>
																<li class="email-icon"> <a href="/component/mailto/?tmpl=component&amp;template=protostar_cmshea&amp;link=c813a72e5e90628edf61037d63c09dbd3b4e28a8" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;" rel="nofollow"><span class="icon-envelope"></span>Email</a> </li>
														</ul>
			</div>
		
	</div>






 <div class="mainFlat">
<p class="story">We are always looking for ways to improve our annual event, the<em><strong> Corporal Christopher M. Shea Memorial 5K.</strong></em> There are several ways that you can help: <a href="#SPONSOR">Sponsor the Event</a>, <a href="#RUN">Participate</a>, <a href="#VOLUNTEER">Volunteer</a> at the event, or provide any other goods and services that will benefit the 5k race.</p>
</div>
<div class="mainFlat">
<h1 id="SPONSER"><strong>Sponsor the Event...</strong></h1>
<p class="story">As an organization, we accept sponsorship of several types and we offer sponsorship levels to suit the needs of every company or individual that would like to be part of our cause.</p>
<p class="story">By becoming one of our sponsors you will know your contribution is helping a mission that has a positive impact on the community - locally, regionally, or nationally.</p>
<p class="story">The levels include both monetary and alternative forms of sponsorship -- i.e. goods and services. If you would like to help out and are unsure if you fit into one of the sponsorship levels, please do not hesitate to <a href="contact.php">contact us</a> with any questions you may have.</p>
<p class="story">If you'd like to sponsor our event, simply <a href="docs/Memorial_5k_Application_2012.pdf">download our application</a>, complete the Application for Sponsorship form, and drop it in the mail today.</p>
<ul>
<li>
<p class="story"><span style="color: #c9960c;"><b> Gold Medalist ($1000): </b></span> Have your Name or Business prominently listed on the welcome banner as a primary sponsor, on the race t-shirt given to all registrants, in ALL publicity, and on the race website as a Gold Medalist sponsor.</p>
</li>
<li>
<p class="story"><span style="color: #808080;"><b> Silver Medalist ($500): </b></span> Have your Name or Business displayed on the race course / park entrance, on the race t-shirt given to all registrants, and on the race website as a Silver Medalist sponsor.</p>
</li>
<li>
<p class="story"><span style="color: #a67d3d;"><b> Bronze Medalist ($150): </b></span> Have your Name or Business listed on the race t-shirt to all registrants, and on the race website as a Bronze Medalist sponsor</p>
</li>
<li>
<p class="story"><b> Race Supporter ($50): </b> Have your Name or Business listed on the race website as a Race Supporter.</p>
</li>
<li>
<p class="story"><b> Alternative Sponsorship: </b> Have your Name or Business listed on the race t-shirt given to all registrants.</p>
</li>
</ul>
</div>
<div class="mainFlat">
<h1>Run, Run, Run...</h1>
<p class="story">The most obvious way you can help out is by participating in the annual 5k race. The entry fee is small - it may actually be the smallest in the Tri-state area. There are t-shirts for all pre-registrants (and onsite registrants while supplies last), food to fuel up before the race or to replenish after the race, water, goodie bags, and any new attractions that crop up in that year.</p>
<p class="story">The course is a scenic double loop through the campgrounds of Swartswood State Park, in Swartswood New Jersey. The run is timed, and you are entered to compete against over 100 runners for overall, as well as against a group of your peers in 7 different age categories. Trophies or medals are awarded to the top 3 runners in all categories.</p>
<p class="story">All proceeds benefit the <b>The Cpl. Christopher M. Shea Scholarship Fund</b>. Online registration is available through <a href="http://www.active.com/5k-race/swartswood-nj/10th-annual-corporal-christopher-m-shea-memorial-5k-2014">active.com</a> or <a href="docs/8x11_app_2014.pdf">download our application</a>, complete the Application for 5k Run form and drop it in the mail.</p>
</div>
<div class="mainFlat">
<h1>Volunteer at the Event</h1>
<p class="story">We are always in need of volunteers - in the weeks before the race, and on race day. You can help us publicize the event in the weeks leading up to race day, or volunteer your services on the day of the run. We are always looking for people to direct runners through the course, hand out gifts and goodies, assist the official timer, or help distribute prizes and awards at the end of the race.</p>
<p class="story">We're open to ANY ideas you may have in this area. Do you have a specialty that will benefit our event on race day? Volunteer your specialized service and this will qualify you for our Alternative Sponsorship (see above).</p>
</div><div class="addtoany_container"><span class="a2a_kit a2a_kit_size_32 addtoany_list" data-a2a-url="http://www.cmshea.org/how-can-i-help" data-a2a-title="How Can I Help?">
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_google_plus"></a>
<a class="a2a_dd" href="http://www.addtoany.com/share_save"></a>
</span>
</div>
		<dl class="article-info muted">

		
			<dt class="article-info-term">
													Details							</dt>

							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
					Written by <span itemprop="name">Patrick Shea</span>	</dd>
			
			
										<dd class="category-name">
																		Category: <a href="/race-results/2-uncategorised" itemprop="genre">Uncategorised</a>							</dd>			
										<dd class="published">
				<span class="icon-calendar"></span>
				<time datetime="2014-07-22T18:50:38+00:00" itemprop="datePublished">
					Published: 22 July 2014				</time>
			</dd>					
					
			
										<dd class="hits">
					<span class="icon-eye-open"></span>
					<meta itemprop="interactionCount" content="UserPageVisits:1295" />
					Hits: 1295			</dd>						</dl>



				</div>
				<!-- end item -->
							</div><!-- end span -->
							</div><!-- end row -->
						
			<div class="items-more">
			

<ol class="nav nav-tabs nav-stacked">
	<li>
		<a href="/race-results">
			Race Results</a>
	</li>
	<li>
		<a href="/photos">
			Photos</a>
	</li>
	<li>
		<a href="/contact-us">
			Contact Us</a>
	</li>
</ol>
		</div>
	
		</div>
";s:4:"head";a:11:{s:5:"title";s:12:"Race Results";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:2:{s:8:"keywords";N;s:6:"rights";N;}}s:5:"links";a:2:{s:54:"/race-results/2-uncategorised?format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:55:"/race-results/2-uncategorised?format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:2:{s:54:"/components/com_jcomments/tpl/default/style.css?v=3002";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:59:"http://www.cmshea.org/plugins/content/addtoany/addtoany.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:1:{s:8:"text/css";s:932:".besps_holder_1_0 {width:700px;height:522px;}
.besps_ctrls_1_0 {display:block;width:700px;padding-top:503px;text-align:right;}
.besps_holder_1_0 {margin:10px;float:left;}
.besps_slides_1_0 {position:absolute;width:700px;height:500px;}
.besps_slides_1_0 div {visibility:hidden;z-index:1;position:absolute;left:0;top:0;width:700px;height:500px;background-color:#FFFFFF;}
.besps_slides_1_0 div img {position:absolute;}
#img_1_0_1 {visibility:visible;z-index:2;}
.besps_holder_1_1 {width:700px;height:522px;}
.besps_ctrls_1_1 {display:block;width:700px;padding-top:503px;text-align:right;}
.besps_holder_1_1 {margin:auto;padding:0;display:block;}
.besps_slides_1_1 {position:absolute;width:700px;height:500px;}
.besps_slides_1_1 div {visibility:hidden;z-index:1;position:absolute;left:0;top:0;width:700px;height:500px;background-color:#FFFFFF;}
.besps_slides_1_1 div img {position:absolute;}
#img_1_1_1 {visibility:visible;z-index:2;}
";}s:7:"scripts";a:8:{s:51:"/components/com_jcomments/js/jcomments-v2.3.js?v=12";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:58:"/components/com_jcomments/libraries/joomlatune/ajax.js?v=4";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:34:"//static.addtoany.com/menu/page.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:54:"/plugins/content/simplepictureslideshow/files/besps.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/jui/js/jquery.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:34:"/media/jui/js/jquery-noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:35:"/media/jui/js/jquery-migrate.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:225:"var besps_1_0= new besps_slideshow("1_0",50,3000,20,1,159,"NOCAPS");var besps_1_1= new besps_slideshow("1_1",50,3000,20,1,164,"NOCAPS");jQuery(window).on('load',  function() {
				new JCaption('img.caption');
			});";}s:6:"custom";a:2:{i:0;s:22:"<!-- besps_count 1 -->";i:1;s:104:"<link href="/plugins/content/simplepictureslideshow/files/besps.css" rel="stylesheet" type="text/css" />";}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:12:"Race Results";s:4:"link";s:20:"index.php?Itemid=128";}i:1;O:8:"stdClass":2:{s:4:"name";s:13:"Uncategorised";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}