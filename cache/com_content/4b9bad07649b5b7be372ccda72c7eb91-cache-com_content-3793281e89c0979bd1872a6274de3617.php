<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:3962:"<div class="item-page" itemscope itemtype="http://schema.org/Article">
	<meta itemprop="inLanguage" content="en-GB" />
	
		
			<div class="page-header">
		<h2 itemprop="name">
							About the Scholarship					</h2>
							</div>
							<div id="pop-print" class="btn hidden-print">
				<a href="#" onclick="window.print();return false;"><span class="icon-print"></span>Print</a>			</div>
			
	
	
		
								<div itemprop="articleBody">
		<div class="mainFlat"><a href="docs/CMShea_Scholarship_App_1.2.pdf">Apply for the Scholarship</a></div>
<div class="mainFlat"><hr /></div>
<h2 class="mainFlat"> </h2>
<table style="margin-left: auto; margin-right: auto;" border="1">
<tbody>
<tr>
<td style="text-align: center;" colspan="3">
<p><span style="font-weight: bold;">Corporal Christopher M. Shea Scholarship Award Recipients</span></p>
</td>
</tr>
<tr>
<td style="text-align: center;">
<p><span style="font-weight: bold; text-align: center;">Year </span></p>
</td>
<td style="text-align: center;">
<p><span style="font-weight: bold; text-align: center;">Recipient</span></p>
</td>
<td style="text-align: center;">
<p><span style="font-weight: bold; text-align: center;">High School</span></p>
</td>
</tr>
<tr>
<td>
<p>2015</p>
</td>
<td>
<p>Jordan Lorenzo</p>
<p>TBD</p>
</td>
<td>
<p><span style="line-height: 15.808px;">Kittatinny Regional, Hampton NJ</span></p>
<p><span style="line-height: 15.808px;">Sussex County Community College</span></p>
</td>
</tr>
<tr>
<td>
<p>2014</p>
</td>
<td>
<p>Blake E. DiGiaimo and Scott H. Sponder</p>
</td>
<td>
<p>Kittatinny Regional, Hampton NJ </p>
</td>
</tr>
<tr>
<td>
<p>2013</p>
</td>
<td>
<p>Jeffrey M. Chandler</p>
</td>
<td>
<p>Kittatinny Regional, Hampton NJ</p>
</td>
</tr>
<tr>
<td>
<p>2012</p>
</td>
<td>
<p>Conner T. Casterline and Jordan P. Murch</p>
</td>
<td>
<p>Kittatinny Regional, Hampton NJ</p>
</td>
</tr>
<tr>
<td>
<p>2011</p>
</td>
<td>
<p>Donated to the Ryan Senkier Fight Cancer Fund  </p>
<p>http://helpryanfight.wordpress.com/</p>
</td>
<td>
<p>Ryan was a 1992 graduate of Kittatinny Regional High School </p>
</td>
</tr>
<tr>
<td>
<p>2010</p>
</td>
<td>
<p>Daniel McCarthy and Emileigh Galinski</p>
</td>
<td>
<p>Kittatinny Regional, Hampton NJ</p>
</td>
</tr>
<tr>
<td>
<p>2009</p>
</td>
<td>
<p>Jeff Latawiec and Stacey Norman</p>
</td>
<td>
<p>Kittatinny Regional, Hampton NJ</p>
</td>
</tr>
<tr>
<td>
<p>2008</p>
</td>
<td>
<p>Jacob Stark</p>
</td>
<td>
<p>Kittatinny Regional, Hampton NJ</p>
</td>
</tr>
<tr>
<td>
<p>2007</p>
</td>
<td>
<p>Zachary Virga</p>
</td>
<td>
<p>Kittatinny Regional, Hampton NJ</p>
</td>
</tr>
</tbody>
</table> <div class="addtoany_container"><span class="a2a_kit a2a_kit_size_32 addtoany_list" data-a2a-url="http://www.cmshea.org/about-the-scholarship" data-a2a-title="About the Scholarship">
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_google_plus"></a>
<a class="a2a_dd" href="http://www.addtoany.com/share_save"></a>
</span>
</div>	</div>

				<dl class="article-info muted">

		
			<dt class="article-info-term">
													Details							</dt>

							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
					Written by <span itemprop="name">Patrick Shea</span>	</dd>
			
			
										<dd class="category-name">
																		Category: <a href="/about-the-scholarship/2-uncategorised" itemprop="genre">Uncategorised</a>							</dd>			
										<dd class="published">
				<span class="icon-calendar"></span>
				<time datetime="2014-07-22T18:46:22+00:00" itemprop="datePublished">
					Published: 22 July 2014				</time>
			</dd>					
					
			
										<dd class="hits">
					<span class="icon-eye-open"></span>
					<meta itemprop="interactionCount" content="UserPageVisits:1525" />
					Hits: 1525			</dd>						</dl>
	
						</div>

";s:4:"head";a:11:{s:5:"title";s:15:"The Scholarship";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:4:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:12:"Patrick Shea";s:6:"robots";s:17:"noindex, nofollow";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:54:"/components/com_jcomments/tpl/default/style.css?v=3002";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:59:"http://www.cmshea.org/plugins/content/addtoany/addtoany.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:7:{s:51:"/components/com_jcomments/js/jcomments-v2.3.js?v=12";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:58:"/components/com_jcomments/libraries/joomlatune/ajax.js?v=4";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:34:"//static.addtoany.com/menu/page.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/jui/js/jquery.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:34:"/media/jui/js/jquery-noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:35:"/media/jui/js/jquery-migrate.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:80:"jQuery(window).on('load',  function() {
				new JCaption('img.caption');
			});";}s:6:"custom";a:0:{}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:15:"The Scholarship";s:4:"link";s:20:"index.php?Itemid=110";}}s:6:"module";a:0:{}}