<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:9484:"<div class="item-page" itemscope itemtype="http://schema.org/Article">
	<meta itemprop="inLanguage" content="en-GB" />
	
		
			<div class="page-header">
		<h2 itemprop="name">
							The Life of a Hero					</h2>
							</div>
							<div id="pop-print" class="btn hidden-print">
				<a href="#" onclick="window.print();return false;"><span class="icon-print"></span>Print</a>			</div>
			
	
	
		
								<div itemprop="articleBody">
		<h1>The Beginning</h1>
<hr />
<p>Corporal Christopher M. Shea was born on Oct. 4th, 1972 to Maurice and Patricia Shea of Jersey City, NJ. He was the second child in a family of eight. Just before he entered into the 4th grade, the family moved to Union City, NJ, where Chris spent his youth days attending St. Joseph's school (4th to the 8th grades). The summer before Chris entered high school, the family chose to leave the urban areas of NJ, and raise the growing family (the 8th, Timothy, was just born) in rural Sussex County, NJ.</p>
<p>Chris attended Kittatinny Regional High School in Hampton, NJ. He ran cross country in the fall, played basketball in the winter, and played baseball in the spring. He loved imposing his techniques as a catcher on anyone that would listen. He graduated in 1990, with a full understanding of his career goals.</p>
<p>Chris' passion to become a police officer started when he was 12. In the eighth grade, Chris was nominated to spend a week at the NJ State Police basic training facility in Sea Girt, and this is what convinced him that this is what he wanted to do. As the end of high school approached, and Chris would have to choose his career direction, is when Chris would begin to turn this dream into a reality. He would first choose Wagner College in Staten Island, NY, where he attempted to study Criminal Justice. Realizing after his first semester that this studying "gig" was not for him, and knowing at the time that the only other path to becoming a NJ State Police Officer without a college degree was 4 years of military service, Chris chose to become one of the few and the proud and enlisted in the United States Marine Corps.</p>
<p> </p>
<h1>The Marines</h1>
<hr />
<p>Chris served 4 glorious years in the Marine Corps. He worked as a security officer in what is now most likely considered Homeland Security. This was fortunate for him and his family, since it limited his assignment to a military base in Jacksonville, FL and he never had to do a global tour or serve over seas during Desert Storm.</p>
<p>Chris loved the Marine Corps, and he served his duty guarding nuclear missiles used on submarines with conviction and pride.</p>
<p> </p>
<h1>His Family</h1>
<hr />
<p>After Chris completed his tour in the military, he took a job as a security officer at Fairleigh Dickinson University in Teaneck, NJ. This worked well, you see, because Chris had come to learn that the requirement for being accepted into NJ State Police had changed while he was serving his 4 years in the Marines. When he enlisted, the requirement was 4 years of military service OR a 4-yr college degree. At this point, the rules were now 4 years of military service AND at least a 2-yr associates degree. Chris quickly enrolled into the FDU curriculum to get started.</p>
<p>Chris knew he would get through this program and earn his degree (I'm not sure he expected to do it with honors), but what he didn't know was that a whole new part of his life would start to take shape at FDU, as well. Her name was Susan Cleaver, a varsity volleyball player at FDU. When she first laid eyes on Chris in the summer of 1995, she emphatically told her twin sister, Shannon, that "I'm going to get that man - you'll see". Did she ever.</p>
<p>Chris proposed to Susan in front of an entire gym of FDU volleyball players and fans. Quickly changing into a suit as the end of the match neared, Chris had his buddies display a HUGE sign reading "Susan, Will You Marry Me" as he approached Susan with roses and an engagement ring. Getting on one knee, Chris proposed and Susan gladly accepted.</p>
<p>Chris and Susan were married on May 15, 1999 in Lewes, DE. Their son, Christopher Jr., was born on January 16, 2001. Just like any other proud father, this filled Chris' passion to have a first born son. Their daughter, Elizabeth, was born on August 8 2003. The spitting image of Chris, she quickly became daddy's little girl.</p>
<p> </p>
<h1>The Delaware State Police</h1>
<hr />
<p>Chris moved to Delaware in the winter in the early part of 1999, shortly after he graduated from Fairleigh. He worked a few odd jobs while waiting through the DE State Police application process -- a security officer at Ames department store and a corrections officer. He finally got his chance in 2000, when he was accepted into DE State Police Academy to be part of the 71st Delaware State Police Recruit Class.</p>
<p>Chris graduated from the Delaware State Police Academy on December 16, 2000. He was first assigned to Troop 5, in Bridgeville DE, off the Sussex Highway and about 30 miles west of where he and Susan lived. Saying he was excited to finally be "on the job" is an understatement. I'll always remember the countless hours talking to Chris about his dream job, or more preciseley listening to him ramble on with story after story of his daily routines.</p>
<p>On one particular call, we all got a BIG scare. Chris was leaving the Troop 5 barracks in response to a call, crossing over Sussex Highway as he exited the barracks. On his way out, his patrol car was broad sided by some sort of delivery van. I don't like to think of Chris as lucky -- instead I think of him as resilient. He ended up with a broken hip and he still managed to serve as a groomsman (well, on crutches) at our oldest brother's wedding. He kept a smile on his face, quick to say to anyone about to inquire that "No, he did not have to pay for the patrol car -- that's what taxes are for".</p>
<p>Chris eventually capitilized on an opportunity to transfer to DE Troop 7, based in Lewes where he and his family lived. Chris welcomed the short commute to work, and loved the idea that he was only a stones throw away from home. Chris was determined to work his way up the ranks, so he continued to accept overtime work whenever it was available, and the fact that he was now so close to home meant that he would make it home shortly after his shift was over. His hard work and dedication paid off in July of 2004 when he was promoted from Trooper First Class to Corporal. Chris was killed that same month.</p>
<p>It was July 18, 2004 at approximately 2 AM, and he was returning home from a late overtime job on his day off. The job was a routine criminal dropoff to the corrections facility in DE, north of Lewes. It was something he figured was a quick few hours of work, no problem. What he didn't expect to encounter was the unthinkable - a driver on the wrong side of the highway.</p>
<p>Chris was travelling southbound on State Route 1 near Route 206 just outside Milford. The other vehicle was headed northbound in the southbound lanes. The car was being operated by a drunk driver, and was fleeing the scene of an accident that occurred shortly before the fatal crash. Chris never saw it coming - he was taken to Milford Memorial Hospital and pronounced dead at 2:54 AM.</p>
<p>Corporal Christopher M. Shea was laid to rest on July 22, 2004 with full military honors. The funeral services were attended by family, friends, troopers and police officers from across the U.S. The services were powerful, like nothing I have ever experienced before and hope to never have to experience again.</p>
<p>Chris is a hero. A hero because he chose a career, a lifestyle, of helping others. He was never afraid of what he was doing, never second guessed himself for one second. During the funeral services, Chris' Academy classmate and friend of 4 years gave a personal and moving <a href="images/eulogy.jpg">eulogy</a>where he gave, perhaps, the best reason for why Chris was a hero. He said "Chris, I believe you are a hero who saved the life of an unknown motorist who would have been travelling southbound on State Route 1 by forfeiting your own life. Your selfless service saved the life of someone you never even met."</p> <div class="addtoany_container"><span class="a2a_kit a2a_kit_size_32 addtoany_list" data-a2a-url="http://www.cmshea.org/chris-story" data-a2a-title="The Life of a Hero">
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_google_plus"></a>
<a class="a2a_dd" href="http://www.addtoany.com/share_save"></a>
</span>
</div>	</div>

				<dl class="article-info muted">

		
			<dt class="article-info-term">
													Details							</dt>

							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
					Written by <span itemprop="name">Patrick Shea</span>	</dd>
			
			
										<dd class="category-name">
																		Category: <a href="/chris-story/2-uncategorised" itemprop="genre">Uncategorised</a>							</dd>			
										<dd class="published">
				<span class="icon-calendar"></span>
				<time datetime="2014-07-18T04:54:12+00:00" itemprop="datePublished">
					Published: 18 July 2014				</time>
			</dd>					
					
			
										<dd class="hits">
					<span class="icon-eye-open"></span>
					<meta itemprop="interactionCount" content="UserPageVisits:2435" />
					Hits: 2435			</dd>						</dl>
	
						</div>

";s:4:"head";a:11:{s:5:"title";s:12:"Chris' Story";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:4:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:12:"Patrick Shea";s:6:"robots";s:17:"noindex, nofollow";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:54:"/components/com_jcomments/tpl/default/style.css?v=3002";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:59:"http://www.cmshea.org/plugins/content/addtoany/addtoany.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:7:{s:51:"/components/com_jcomments/js/jcomments-v2.3.js?v=12";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:58:"/components/com_jcomments/libraries/joomlatune/ajax.js?v=4";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:34:"//static.addtoany.com/menu/page.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/jui/js/jquery.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:34:"/media/jui/js/jquery-noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:35:"/media/jui/js/jquery-migrate.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:80:"jQuery(window).on('load',  function() {
				new JCaption('img.caption');
			});";}s:6:"custom";a:0:{}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:12:"Chris' Story";s:4:"link";s:20:"index.php?Itemid=108";}}s:6:"module";a:0:{}}