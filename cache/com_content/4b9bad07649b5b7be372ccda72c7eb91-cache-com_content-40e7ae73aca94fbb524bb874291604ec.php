<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:11112:"<div class="item-page" itemscope itemtype="http://schema.org/Article">
	<meta itemprop="inLanguage" content="en-GB" />
	
		
			<div class="page-header">
		<h2 itemprop="name">
							Race Results					</h2>
							</div>
							
<div class="icons">
	
					<div class="btn-group pull-right">
				<a class="btn dropdown-toggle" data-toggle="dropdown" href="#"> <span class="icon-cog"></span><span class="caret"></span> </a>
								<ul class="dropdown-menu">
											<li class="print-icon"> <a href="/race-results?tmpl=component&amp;print=1&amp;page=" title="Print" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon-print"></span>Print</a> </li>
																<li class="email-icon"> <a href="/component/mailto/?tmpl=component&amp;template=protostar_cmshea&amp;link=638ca10befdae0050bcf55cb4c6dcae14fca5b8e" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;" rel="nofollow"><span class="icon-envelope"></span>Email</a> </li>
														</ul>
			</div>
		
	</div>
			
	
	
		
								<div itemprop="articleBody">
		<p style="font-size: 8px;"><i><mark style="background-color: #ccffcc;">Note: we're working on getting the results database from all years integrated into our new website. We'll let you know when it's available.</mark></i></p>
<h1><b>                 10th Annual Christopher Shea Memorial 5K</b></h1>
<h1><b>                              October 5, 2014</b></h1>
<pre style="font-size: 16px;"><b>Place Name             Age Sex          Badge       Time  Pace </b></pre>
<hr />
<pre style="font-size: 14px;"><b>    1 Rob Rohel            32 M               NJSP North   18:56  6:07</b></pre>
<pre style="font-size: 14px;"><b>    2 James Kline          42 M                            19:02  6:09 </b></pre>
<pre style="font-size: 14px;"><b>    3 Brian Santos         31 M               NJSP North   19:36  6:20</b></pre>
<pre style="font-size: 14px;"><b>    4 Tommy Ferrigno       37 M               NJSP North   19:45  6:22</b></pre>
<pre style="font-size: 14px;"><b>    5 Christopher Ruiz     13 M                            20:09  6:30 </b></pre>
<pre style="font-size: 14px;"><b>    6 Mark Moyna           51 M               Sussex St.   20:30  6:37</b></pre>
<pre style="font-size: 14px;"><b>    7 Justin DeLorenzo     30 M               Sussex St.   21:22  6:54</b></pre>
<pre style="font-size: 14px;"><b>    8 Tim Sanderson        27 M               Sussex St.   21:35  6:58</b></pre>
<pre style="font-size: 14px;"><b>    9 Nick Kazimierczak    25 M                            21:54  7:04 </b></pre>
<pre style="font-size: 14px;"><b>   10 Christine Chapman    36 F                            22:21  7:13 </b></pre>
<pre style="font-size: 14px;"><b>   11 Jose Gonzalo         26 M               Sussex St.   22:38  7:19</b></pre>
<pre style="font-size: 14px;"><b>   12 Danielle Ferrigno    35 F                            22:39  7:19 </b></pre>
<pre style="font-size: 14px;"><b>   13 Patrick Graffius     39 M                            22:46  7:21 </b></pre>
<pre style="font-size: 14px;"><b>   14 Daniel Pidgeon       19 M                            22:49  7:22 </b></pre>
<pre style="font-size: 14px;"><b>   15 Carmen Liuzza        12 M                            22:57  7:25 </b></pre>
<pre style="font-size: 14px;"><b>   16 James Ruiz           47 M                            23:00  7:25 </b></pre>
<pre style="font-size: 14px;"><b>   17 Brian Haczyk         28 M               Sussex St.   23:22  7:33</b></pre>
<pre style="font-size: 14px;"><b>   18 Ryan Espinosa        09 M                            23:41  7:39 </b></pre>
<pre style="font-size: 14px;"><b>   19 Radka Pieroni        37 F                            23:53  7:43 </b></pre>
<pre style="font-size: 14px;"><b>   20 Dylan Zupkay         08 M                            24:01  7:45 </b></pre>
<pre style="font-size: 14px;"><b>   21 Andrew Stephanic     25 M               Sussex St.   24:04  7:46 </b></pre>
<pre style="font-size: 14px;"><b>   22 Mark Moyna           11 M                            24:17  7:50 </b></pre>
<pre style="font-size: 14px;"><b>   23 Ashley Espinosa      11 F                            24:24  7:53 </b></pre>
<pre style="font-size: 14px;"><b>   24 Josh Gallant         09 M                            24:25  7:53 </b></pre>
<pre style="font-size: 14px;"><b>   25 Casey Kazimierczak   53 M                            24:26  7:53 </b></pre>
<pre style="font-size: 14px;"><b>   26 Daniel Stevans       38 M                            24:28  7:54 </b></pre>
<pre style="font-size: 14px;"><b>   27 Randy Smolinski      41 M                            24:38  7:57 </b></pre>
<pre style="font-size: 14px;"><b>   28 Tim Shea             28 M                            24:39  7:57 </b></pre>
<pre style="font-size: 14px;"><b>   29 Brad V. Howlett      25 M                            24:49  8:01 </b></pre>
<pre style="font-size: 14px;"><b>   30 Frank Hurtado        35 M               Sussex St.   25:15  8:09 </b></pre>
<pre style="font-size: 14px;"><b>   31 Ryan Phillips        28 M                            25:32  8:15 </b></pre>
<pre style="font-size: 14px;"><b>   32 Michael Bracciofort  34 M                            25:35  8:16 </b></pre>
<pre style="font-size: 14px;"><b>   33 Andrew Shea          29 M               DSP          25:36  8:16</b></pre>
<pre style="font-size: 14px;"><b>   34 Alexa Zupkay         11 F                            25:55  8:22 </b></pre>
<pre style="font-size: 14px;"><b>   35 Anthony Batko        61 M                            26:12  8:27 </b></pre>
<pre style="font-size: 14px;"><b>   36 Susan Cipriano       37 F                            26:23  8:31 </b></pre>
<pre style="font-size: 14px;"><b>   37 Trey Gallant         08 M                            26:31  8:34 </b></pre>
<pre style="font-size: 14px;"><b>   38 Alex Cameron         29 M                            26:42  8:37 </b></pre>
<pre style="font-size: 14px;"><b>   39 Pete Woz             45 M                            26:50  8:40 </b></pre>
<pre style="font-size: 14px;"><b>   40 Tara Phillips        27 F                            27:10  8:46 </b></pre>
<pre style="font-size: 14px;"><b>   41 Abraham Garib        31 M               Sussex St.   27:23  8:50 </b></pre>
<pre style="font-size: 14px;"><b>   42 Erin Barry           36 F                            27:25  8:51 </b></pre>
<pre style="font-size: 14px;"><b>   43 Daniel Buckelew      58 M                            27:47  8:58 </b></pre>
<pre style="font-size: 14px;"><b>   44 Martha Feehan        40 F                            27:50  8:59 </b></pre>
<pre style="font-size: 14px;"><b>   45 Matt Oras            29 M                            28:40  9:15 </b></pre>
<pre style="font-size: 14px;"><b>   46 William FISCHER      20 M                            29:20  9:28 </b></pre>
<pre style="font-size: 14px;"><b>   47 Joseph Corleto       26 M                            29:23  9:29 </b></pre>
<pre style="font-size: 14px;"><b>   48 Kelly Graffius       38 F                            29:50  9:38 </b></pre>
<pre style="font-size: 14px;"><b>   49 Erin Sobczak         36 F                            29:55  9:39 </b></pre>
<pre style="font-size: 14px;"><b>   50 David Jennings       20 M                            29:55  9:39 </b></pre>
<pre style="font-size: 14px;"><b>   51 Mackenzie Shea       11 F                            30:15  9:46 </b></pre>
<pre style="font-size: 14px;"><b>   52 Wendy Cooney         54 F                            31:14 10:05 </b></pre>
<pre style="font-size: 14px;"><b>   53 Terence Degnon       52 M                            31:21 10:07 </b></pre>
<pre style="font-size: 14px;"><b>   54 Liz Yeager           51 F                            31:47 10:15 </b></pre>
<pre style="font-size: 14px;"><b>   55 Mason Gallant        11 M                            31:59 10:19 </b></pre>
<pre style="font-size: 14px;"><b>   56 Bill McGovern        61 M                            32:41 10:33 </b></pre>
<pre style="font-size: 14px;"><b>   57 Korina Campos        18 F                            38:17 12:21 </b></pre>
<pre style="font-size: 14px;"><b>   58 Stephanie DeLadsa    25 F                            38:25 12:24 </b></pre> <div class="addtoany_container"><span class="a2a_kit a2a_kit_size_32 addtoany_list" data-a2a-url="http://www.cmshea.org/race-results" data-a2a-title="Race Results">
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_google_plus"></a>
<a class="a2a_dd" href="http://www.addtoany.com/share_save"></a>
</span>
</div>	</div>

				<dl class="article-info muted">

		
			<dt class="article-info-term">
													Details							</dt>

							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
					Written by <span itemprop="name">Patrick Shea</span>	</dd>
			
			
										<dd class="category-name">
																		Category: <a href="/race-results/2-uncategorised" itemprop="genre">Uncategorised</a>							</dd>			
										<dd class="published">
				<span class="icon-calendar"></span>
				<time datetime="2014-07-25T20:13:18+00:00" itemprop="datePublished">
					Published: 25 July 2014				</time>
			</dd>					
					
			
										<dd class="hits">
					<span class="icon-eye-open"></span>
					<meta itemprop="interactionCount" content="UserPageVisits:1585" />
					Hits: 1585			</dd>						</dl>
	
	
<ul class="pager pagenav">
	<li class="previous">
		<a href="/how-can-i-help" rel="prev">
			<i class="icon-chevron-left"></i> Prev		</a>
	</li>
	<li class="next">
		<a href="/photos" rel="next">
			Next <i class="icon-chevron-right"></i>		</a>
	</li>
</ul>
						</div>

";s:4:"head";a:11:{s:5:"title";s:12:"Race Results";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:12:"Patrick Shea";}}s:5:"links";a:0:{}s:11:"styleSheets";a:2:{s:54:"/components/com_jcomments/tpl/default/style.css?v=3002";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:59:"http://www.cmshea.org/plugins/content/addtoany/addtoany.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:7:{s:51:"/components/com_jcomments/js/jcomments-v2.3.js?v=12";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:58:"/components/com_jcomments/libraries/joomlatune/ajax.js?v=4";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:34:"//static.addtoany.com/menu/page.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/jui/js/jquery.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:34:"/media/jui/js/jquery-noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:35:"/media/jui/js/jquery-migrate.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:80:"jQuery(window).on('load',  function() {
				new JCaption('img.caption');
			});";}s:6:"custom";a:0:{}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:12:"Race Results";s:4:"link";s:20:"index.php?Itemid=128";}}s:6:"module";a:0:{}}