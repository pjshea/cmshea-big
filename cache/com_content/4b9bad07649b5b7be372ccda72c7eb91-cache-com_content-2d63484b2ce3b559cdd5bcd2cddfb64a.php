<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:7240:"<div class="item-page" itemscope itemtype="http://schema.org/Article">
	<meta itemprop="inLanguage" content="en-GB" />
	
		
			<div class="page-header">
		<h2 itemprop="name">
							How Can I Help?					</h2>
							</div>
							
<div class="icons">
	
					<div class="btn-group pull-right">
				<a class="btn dropdown-toggle" data-toggle="dropdown" href="#"> <span class="icon-cog"></span><span class="caret"></span> </a>
								<ul class="dropdown-menu">
											<li class="print-icon"> <a href="/how-can-i-help?tmpl=component&amp;print=1&amp;page=" title="Print" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><span class="icon-print"></span>Print</a> </li>
																<li class="email-icon"> <a href="/component/mailto/?tmpl=component&amp;template=protostar_cmshea&amp;link=c813a72e5e90628edf61037d63c09dbd3b4e28a8" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;" rel="nofollow"><span class="icon-envelope"></span>Email</a> </li>
														</ul>
			</div>
		
	</div>
			
	
	
		
								<div itemprop="articleBody">
		<div class="mainFlat">
<p class="story">We are always looking for ways to improve our annual event, the<em><strong> Corporal Christopher M. Shea Memorial 5K.</strong></em> There are several ways that you can help: <a href="#SPONSOR">Sponsor the Event</a>, <a href="#RUN">Participate</a>, <a href="#VOLUNTEER">Volunteer</a> at the event, or provide any other goods and services that will benefit the 5k race.</p>
</div>
<div class="mainFlat">
<h1 id="SPONSER"><strong>Sponsor the Event...</strong></h1>
<p class="story">As an organization, we accept sponsorship of several types and we offer sponsorship levels to suit the needs of every company or individual that would like to be part of our cause.</p>
<p class="story">By becoming one of our sponsors you will know your contribution is helping a mission that has a positive impact on the community - locally, regionally, or nationally.</p>
<p class="story">The levels include both monetary and alternative forms of sponsorship -- i.e. goods and services. If you would like to help out and are unsure if you fit into one of the sponsorship levels, please do not hesitate to <a href="contact.php">contact us</a> with any questions you may have.</p>
<p class="story">If you'd like to sponsor our event, simply <a href="docs/Memorial_5k_Application_2012.pdf">download our application</a>, complete the Application for Sponsorship form, and drop it in the mail today.</p>
<ul>
<li>
<p class="story"><span style="color: #c9960c;"><b> Gold Medalist ($1000): </b></span> Have your Name or Business prominently listed on the welcome banner as a primary sponsor, on the race t-shirt given to all registrants, in ALL publicity, and on the race website as a Gold Medalist sponsor.</p>
</li>
<li>
<p class="story"><span style="color: #808080;"><b> Silver Medalist ($500): </b></span> Have your Name or Business displayed on the race course / park entrance, on the race t-shirt given to all registrants, and on the race website as a Silver Medalist sponsor.</p>
</li>
<li>
<p class="story"><span style="color: #a67d3d;"><b> Bronze Medalist ($150): </b></span> Have your Name or Business listed on the race t-shirt to all registrants, and on the race website as a Bronze Medalist sponsor</p>
</li>
<li>
<p class="story"><b> Race Supporter ($50): </b> Have your Name or Business listed on the race website as a Race Supporter.</p>
</li>
<li>
<p class="story"><b> Alternative Sponsorship: </b> Have your Name or Business listed on the race t-shirt given to all registrants.</p>
</li>
</ul>
</div>
<div class="mainFlat">
<h1>Run, Run, Run...</h1>
<p class="story">The most obvious way you can help out is by participating in the annual 5k race. The entry fee is small - it may actually be the smallest in the Tri-state area. There are t-shirts for all pre-registrants (and onsite registrants while supplies last), food to fuel up before the race or to replenish after the race, water, goodie bags, and any new attractions that crop up in that year.</p>
<p class="story">The course is a scenic double loop through the campgrounds of Swartswood State Park, in Swartswood New Jersey. The run is timed, and you are entered to compete against over 100 runners for overall, as well as against a group of your peers in 7 different age categories. Trophies or medals are awarded to the top 3 runners in all categories.</p>
<p class="story">All proceeds benefit the <b>The Cpl. Christopher M. Shea Scholarship Fund</b>. Online registration is available through <a href="http://www.active.com/5k-race/swartswood-nj/10th-annual-corporal-christopher-m-shea-memorial-5k-2014">active.com</a> or <a href="docs/8x11_app_2014.pdf">download our application</a>, complete the Application for 5k Run form and drop it in the mail.</p>
</div>
<div class="mainFlat">
<h1>Volunteer at the Event</h1>
<p class="story">We are always in need of volunteers - in the weeks before the race, and on race day. You can help us publicize the event in the weeks leading up to race day, or volunteer your services on the day of the run. We are always looking for people to direct runners through the course, hand out gifts and goodies, assist the official timer, or help distribute prizes and awards at the end of the race.</p>
<p class="story">We're open to ANY ideas you may have in this area. Do you have a specialty that will benefit our event on race day? Volunteer your specialized service and this will qualify you for our Alternative Sponsorship (see above).</p>
</div> <div class="addtoany_container"><span class="a2a_kit a2a_kit_size_32 addtoany_list" data-a2a-url="http://www.cmshea.org/how-can-i-help" data-a2a-title="How Can I Help?">
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_google_plus"></a>
<a class="a2a_dd" href="http://www.addtoany.com/share_save"></a>
</span>
</div>	</div>

				<dl class="article-info muted">

		
			<dt class="article-info-term">
													Details							</dt>

							<dd class="createdby" itemprop="author" itemscope itemtype="http://schema.org/Person">
					Written by <span itemprop="name">Patrick Shea</span>	</dd>
			
			
										<dd class="category-name">
																		Category: <a href="/how-can-i-help/2-uncategorised" itemprop="genre">Uncategorised</a>							</dd>			
										<dd class="published">
				<span class="icon-calendar"></span>
				<time datetime="2014-07-22T18:50:38+00:00" itemprop="datePublished">
					Published: 22 July 2014				</time>
			</dd>					
					
			
										<dd class="hits">
					<span class="icon-eye-open"></span>
					<meta itemprop="interactionCount" content="UserPageVisits:1295" />
					Hits: 1295			</dd>						</dl>
	
	
<ul class="pager pagenav">
	<li class="previous">
		<a href="/about-the-scholarship" rel="prev">
			<i class="icon-chevron-left"></i> Prev		</a>
	</li>
	<li class="next">
		<a href="/race-results" rel="next">
			Next <i class="icon-chevron-right"></i>		</a>
	</li>
</ul>
						</div>

";s:4:"head";a:11:{s:5:"title";s:15:"How Can I Help?";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:12:"Patrick Shea";}}s:5:"links";a:1:{s:36:"http://www.cmshea.org/how-can-i-help";a:3:{s:8:"relation";s:9:"canonical";s:7:"relType";s:3:"rel";s:7:"attribs";a:0:{}}}s:11:"styleSheets";a:2:{s:54:"/components/com_jcomments/tpl/default/style.css?v=3002";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:59:"http://www.cmshea.org/plugins/content/addtoany/addtoany.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:7:{s:51:"/components/com_jcomments/js/jcomments-v2.3.js?v=12";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:58:"/components/com_jcomments/libraries/joomlatune/ajax.js?v=4";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:34:"//static.addtoany.com/menu/page.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/jui/js/jquery.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:34:"/media/jui/js/jquery-noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:35:"/media/jui/js/jquery-migrate.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:80:"jQuery(window).on('load',  function() {
				new JCaption('img.caption');
			});";}s:6:"custom";a:0:{}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:15:"How Can I Help?";s:4:"link";s:20:"index.php?Itemid=111";}}s:6:"module";a:0:{}}