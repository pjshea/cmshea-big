<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:3930:"<div class="item-page" itemscope itemtype="http://schema.org/Article">
	<meta itemprop="inLanguage" content="en-GB" />
	
		
			<div id="pop-print" class="btn hidden-print">
			<a href="#" onclick="window.print();return false;">Print</a>		</div>
		<div class="clearfix"> </div>
			<div class="page-header">
		<h2 itemprop="name">
							11th Annual Corporal Christopher M. Shea Memorial 5k Recap					</h2>
							</div>
					
	
	
		
								<div itemprop="articleBody">
		<p><img src="images/pictures/2015/IMG_3042.JPG" alt="" width="1050" height="700" /></p>
<p style="font-size: 12.16px; line-height: 15.808px;">The <strong>11th Annual Corporal Christopher M. Shea Memorial 5k</strong> was held on October 4, 2015 in<strong> </strong>Swartswood State Park. The event was a success, with 100+ runners and walkers, and spectators filling the pavilion to cheer on the race finishers. It was a gorgeous day for a run, jog, or even a walk through the park.</p>
<p style="font-size: 12.16px; line-height: 15.808px;">The top 3 overall male finishers were: Rob Rohel from Wantage NJ (18:53), Jiim Kline from Blairstown NJ (19:12), and Carmen Liuzza (19:45).</p>
<p style="font-size: 12.16px; line-height: 15.808px;"><span style="line-height: 15.8079996109009px;">The top 3 overall female finishers were: Christine Chapman from Hopatcong NJ (22:12), Meghan Radimer (22:57), and Ashley Espinosa (23:10).</span></p>
<p style="font-size: 12.16px; line-height: 15.808px;">We'd like to also say that the competition for the <b>Battle of the Badges</b> this awesome. There were runners from the NJ State Police and the Delaware State Police competing to take home the coveted <strong>Battle of the Badges</strong> plaque. In the end, the team of Rob Rohel, Justin DeLorenzo, and Andrew Stephanic of the NJSP Field Ops out the DE State Police score of 17-19 (lowest score wins).</p>
<p style="font-size: 12.16px; line-height: 15.808px;">Congratulations to our top-finishers, all our winners, and everyone that came out to run, jog, walk, spectate, and volunteer in support of the <strong>Corporal Christopher M. Shea Memorial Scholarship Fund</strong> - thank you for making this year competitive, fun, exciting, and for helping us raise over $4,000 to be used as scholarships for students from Kittatinny Regional High School, Sussex County Community College, and other area schools entering a career in law enforcement or as other civil servants.</p>
<p style="font-size: 12.16px; line-height: 15.808px;">We'd also like to thank all of our sponsors and partners - without the help of our primary sponsors Daidone Electric, Inc. and Accomplitech, and all other donors and sponsors we wouldn't have been able to keep this race alive for these past 10 years.  </p>
<p style="color: blue;">Mark your calendars now for next year! </p>
<div id="header" style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 18px;"><strong>12th Annual <span style="font-family: Tahoma, Helvetica, Arial, sans-serif; font-size: 12px; line-height: 1.3em;">Corporal Christopher M. Shea Memorial 5k</span></strong></div>
<div style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 18px;"><strong>When: </strong>Sunday, October 2nd 2016 @ 1 PM</div>
<div style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; line-height: 18px;"><strong>Where: </strong>Swartswood State Park, Swartswood, NJ</div> <div class="addtoany_container"><span class="a2a_kit a2a_kit_size_32 addtoany_list" data-a2a-url="http://www.cmshea.org/" data-a2a-title="11th Annual Corporal Christopher M. Shea Memorial 5k Recap">
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_google_plus"></a>
<a class="a2a_dd" href="http://www.addtoany.com/share_save"></a>
</span>
</div>	</div>

	
						</div>

";s:4:"head";a:11:{s:5:"title";s:4:"Home";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:4:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:12:"Patrick Shea";s:6:"robots";s:17:"noindex, nofollow";}}s:5:"links";a:1:{s:22:"http://www.cmshea.org/";a:3:{s:8:"relation";s:9:"canonical";s:7:"relType";s:3:"rel";s:7:"attribs";a:0:{}}}s:11:"styleSheets";a:2:{s:54:"/components/com_jcomments/tpl/default/style.css?v=3002";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}s:59:"http://www.cmshea.org/plugins/content/addtoany/addtoany.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:7:{s:51:"/components/com_jcomments/js/jcomments-v2.3.js?v=12";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:58:"/components/com_jcomments/libraries/joomlatune/ajax.js?v=4";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:34:"//static.addtoany.com/menu/page.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/jui/js/jquery.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:34:"/media/jui/js/jquery-noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:35:"/media/jui/js/jquery-migrate.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:80:"jQuery(window).on('load',  function() {
				new JCaption('img.caption');
			});";}s:6:"custom";a:0:{}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:0:{}s:6:"module";a:0:{}}