<?php
/*------------------------------------------------------------------------
# plg_addcustomphp - Add Custom PHP
# ------------------------------------------------------------------------
# version 1.0.0
# author Impression eStudio
# copyright Copyright (C) 2013 Impression eStudio. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://joomla.impression-estudio.gr
# Technical Support: info@impression-estudio.gr
-------------------------------------------------------------------------*/

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

jimport( 'joomla.plugin.plugin' );

class plgSystemAddCustomPHP extends JPlugin
{	
	function plgSystemAddCustomPHP( &$subject, $params )
	{
		parent::__construct($subject, $params);

	}
	
	function onAfterInitialise()
	{
		if (strcmp($this->params->get('onafterinitialise_custom_php_file_path'),'')!=0)
		{
			$custom_javascript_file_path = JPATH_SITE.DS.$this->params->get('onafterinitialise_custom_php_file_path');
			ob_start();
			include($custom_javascript_file_path);
			$php = ob_get_contents();
			eval($php);
			ob_end_clean();
		}
	}
	
	function onAfterRoute()
	{
		if (strcmp($this->params->get('onafterroute_custom_php_file_path'),'')!=0)
		{
			$custom_javascript_file_path = JPATH_SITE.DS.$this->params->get('onafterroute_custom_php_file_path');
			ob_start();
			include($custom_javascript_file_path);
			$php = ob_get_contents();
			eval($php);
			ob_end_clean();
		}
	}
	
	function onAfterDispatch()
	{
		if (strcmp($this->params->get('onafterdispatch_custom_php_file_path'),'')!=0)
		{
			$custom_javascript_file_path = JPATH_SITE.DS.$this->params->get('onafterdispatch_custom_php_file_path');
			ob_start();
			include($custom_javascript_file_path);
			$php = ob_get_contents();
			eval($php);
			ob_end_clean();
		}
	}
	
	function onBeforeRender()
	{
		if (strcmp($this->params->get('onbeforerender_custom_php_file_path'),'')!=0)
		{
			$custom_javascript_file_path = JPATH_SITE.DS.$this->params->get('onbeforerender_custom_php_file_path');
			ob_start();
			include($custom_javascript_file_path);
			$php = ob_get_contents();
			eval($php);
			ob_end_clean();
		}
	}
	
	function onAfterRender()
	{
		if (strcmp($this->params->get('onafterrender_custom_php_file_path'),'')!=0)
		{
			$custom_javascript_file_path = JPATH_SITE.DS.$this->params->get('onafterrender_custom_php_file_path');
			ob_start();
			include($custom_javascript_file_path);
			$php = ob_get_contents();
			eval($php);
			ob_end_clean();
		}
	}
	
	function onBeforeCompileHead()
	{
		if (strcmp($this->params->get('onBeforeCompileHead_custom_php_file_path'),'')!=0)
		{
			$custom_javascript_file_path = JPATH_SITE.DS.$this->params->get('onBeforeCompileHead_custom_php_file_path');
			ob_start();
			include($custom_javascript_file_path);
			$php = ob_get_contents();
			eval($php);
			ob_end_clean();
		}
	}
}
